package 高级特性.IO流.输入流;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Demo {
    private static String filePath1 = "src\\高级特性\\IO流\\文件\\file1.txt";

    public static void main(String[] args) {
        // 获取 相对路径，所有java.io中的类都将相对路径解释为 用户工作目录开始 本例： java_datastructure
        System.out.println(System.getProperty("user.dir")); //
        System.out.println(System.getProperties()); //可以获得系统的很多属性，有需要的可以看

        read1();
        read2();

        read3();
    }

    /**
     * inputStream 的方法：read 返回一个unicode 码元，0--65535之间
     * int available();返回在可读入的字节数，
     * int read(),读入一个字节并返回，在结尾返回-1，英文和数字 1 个，回车2个，汉子3个 字节
     */
    public static void read1() {
        try {
            FileInputStream in = new FileInputStream(new File(filePath1));
            int a = in.available();
            System.out.println(a);
            int value = in.read();
            int count = 1;
            while (value != -1) {
                System.out.println(count++ + ":" + value);
                value = in.read();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void read2() {
        try {
            FileInputStream in = new FileInputStream(new File(filePath1));
            int a = in.available();
            System.out.println(a);
            byte[] bytes = new byte[512];
            int value;
            int count = 1;
            String str = "";
            while ((value = in.read(bytes)) > 0) {
                System.out.println(count++ + ":" + value);
                str += new String(bytes, 0, value);
            }
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void read3() {
        try {
            String str = new String(Files.readAllBytes(Paths.get(filePath1)), StandardCharsets.UTF_8);
            System.out.println(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
