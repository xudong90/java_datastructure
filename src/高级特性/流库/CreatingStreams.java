package 高级特性.流库;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CreatingStreams {
    private static String path = "src/高级特性/流库/file1.txt";

    /**
     * 遍历流
     *
     * @param title
     * @param stream
     * @param <T>
     */
    public static <T> void show(String title, Stream<T> stream) {
        final int SIZE = 10;
        List<T> firstElements = stream.limit(SIZE + 1).collect(Collectors.toList());
        System.out.println(title + ":");
        for (int i = 0; i < firstElements.size(); i++) {
            if (i > 0) System.out.print("、");
            if (i < SIZE) System.out.print(firstElements.get(i));
            else System.out.print("...");
        }
        System.out.println();
    }

    /**
     * @param s boat
     * @return ["b","o","a","t"]
     */
    public static Stream<String> letters(String s) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            result.add(s.substring(i, i + 1));
        }
        return result.stream();
    }

    public static String getDefault() {
        return "default";
    }

    public static void print(String str) {
        System.out.println(str);
    }

    public static Stream<Person> people() {
        return Stream.of(new Person(1, "张三"), new Person(2, "李四"), new Person(3, "王五"));
    }

    public static void main(String[] args) {
        /**
         * filter(条件),产生一个流，包括满足条件的元素
         * count() ,计算元素个数，这是一个终止操作
         * stream 和 parallelStream，产生当前元素的顺序流和并行流
         */
        Integer[] array = {1, 3, 5, 55, 32, 12, 4, 38, 6, 7, 89, 12, 1, 3, 3, 55};
        List<Integer> list = Arrays.asList(array);
        long count = list.stream().filter(obj -> obj > 12).count();
        System.out.println(count);
        count = list.parallelStream().filter(obj -> obj > 12).count();
        System.out.println(count);
        /**
         * 流的各种创建方式
         */
        // 1. Stream.of();
        String contents = "12，3,4,6,23,45,66,77,888,17";
        Stream<String> words = Stream.of(contents.split(",")); // 数组
        show("words", words);
        Stream<String> songs = Stream.of("张三", "李四", "王五", "tony"); // 任意数量引用元
        show("songs", songs);
        // 2. 不包含元素的流 Stream.empty()
        Stream<String> empty = Stream.empty();
        show("empty", empty);
        // 3 创建无限流 Stream.generate() 通过反复调用函数实现的
        Stream<String> echos = Stream.generate(() -> "Echo"); // 只有一个常数值的无限流
        show("echos", echos);
        Stream<Double> random = Stream.generate(Math::random); // 随机数流
        show("random", random);
        // 4 创建无限流 产生一个 0,1,2,3 的序列，一个种子，一个在种子调用下产生的值
        Stream<BigInteger> integers = Stream.iterate(BigInteger.ZERO, m -> m.add(BigInteger.ONE));
        show("integers", integers);
        // 5 nio 的path, Files.lines(Path path),包括所有行的流
        Path path1 = Paths.get(path);
        try {
            Stream<String> lines = (Stream<String>) Files.lines(path1);
            show("lines", lines);
            lines = (Stream<String>) Files.lines(path1, StandardCharsets.UTF_8);// 指定编码
            show("lines", lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * filter ,map, flatMap
         */
        try {
            String str = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
            List<String> wordList = Arrays.asList(str.split("\\PL+")); // 非字母分割
            show("wordList", wordList.stream());

            // 1 filter,与某种元素匹配，产生新的流
            Stream<String> longWords = wordList.stream().filter(m -> m.length() > 12);
            show("longWords", longWords);
            // 2 map : 通过某种方式转换流中的值，会有一个函数应用到每个元素上
            longWords = wordList.stream().filter(m -> m.length() > 12).map(String::toUpperCase); //大写
            show("longWords", longWords);
            Stream<String> firstLetters = wordList.stream().map(m -> m.substring(0, 1)); // 首个字符
            show("firstLetters", firstLetters);
            // 3. flatMap,将每个元素的函数产生的流连到一起
            Stream<Stream<String>> streamStream = wordList.stream().filter(m -> m.length() > 12).map(w -> letters(w));
            show("streamStream", streamStream);
            // 上面map里的函数返回的流，得到结果是包含流的流,使用flatMap就摊平为 String  流
            longWords = wordList.stream().filter(m -> m.length() > 12).flatMap(m -> letters(m));
            show("longWords", longWords);

        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * 抽取子流和连接流)
         */
        // limit（long maxSize） 产生一个流，包含了当前流的最初的maxSize个元素,maxSize是下标
        Stream<Double> limitStream = Stream.generate(Math::random).limit(8);
        show("limitStream", limitStream); // 包含8个随机数的流
        // skip(),与limit相反，会丢弃前几个元素
        contents = "12，3,4,6,23,45,66,77,888,17";
        Stream<String> skipStream = Stream.of(contents.split(",")).skip(5);
        show("skipStream", skipStream);
        // 连接流 contact
        Stream<String> concatStream = Stream.concat(letters("Hello"), letters("World"));
        show("concatStream", concatStream);
        /**
         * 其他流的转换
         */
        // distinct(),去除重复的元素
        Stream<String> distinctStream = Stream.of("tom", "tom", "周旭东", "周旭东").distinct();
        show("distinctStream", distinctStream);
        // sorted(),排序 接受 Comparable元素
        Stream<Integer> integerStream = Stream.of(12, 34, 1, 3, 4, 90).sorted();
        show("integerStream", integerStream);
        Stream<String> strStream = Stream.of("23", "44", "1", "a", "b", "ab").sorted();
        show("strStream", strStream);
        // sorted(),接受 Comparator元素,resvered()是倒序
        Stream<String> sortLength = Stream.of("apppp", "t", "abde", "abc", "d", "gh").sorted(Comparator.comparing(String::length).reversed());
        show("sortLength", sortLength);

        /**
         * 简单约简：从流中获取数据答案，是一种终结操作,即执行完这个流就不能用了
         */
        // count(),元素个数
        Stream<Integer> integerStream1 = Stream.of(12, 1, 2, 3, 6, 23);
        Long cout = integerStream1.count();
        System.out.println("count():\n" + cout);
        // max（）和 min(),最大和最小,如果流空，产生空的Opringal对象
        integerStream1 = Stream.of(12, 1, 2, 3, 6, 23);
        Optional<Integer> max = integerStream1.max(Comparator.comparing(Integer::intValue));
        System.out.println("max():\n" + max.get());
        integerStream1 = Stream.of(12, 1, 2, 3, 6, 23);
        Optional<Integer> min = integerStream1.min(Comparator.comparing(Integer::intValue));
        System.out.println("min():\n" + min.get());
        // findFirst(),findAny() 返回第一个和任意一个元素，如果流控，产生空的对象
        Optional<Integer> first = Stream.of(3, 12, 1, 2, 3, 6, 23).findFirst();
        Optional<Integer> any = Stream.of(3, 12, 1, 2, 3, 6, 23).findAny();
        System.out.println("findFirst():\n" + first.get());
        System.out.println("findAny():\n" + any.get());
        // anyMatch(),任意元素之一，allMatch(),所有元素，noneMatch(),没有元素，满足条件
        boolean allMatch = Stream.of(12, 2, 3, 6, 23).allMatch(m -> m > 1);
        System.out.println(allMatch);
        boolean anyMatch = Stream.of(12, 2, 3, 6, 23).anyMatch(m -> m > 6);
        System.out.println(anyMatch);
        boolean noneMatch = Stream.of(12, 2, 3, 6, 23).noneMatch(m -> m > 199);
        System.out.println(noneMatch);
        /**
         * Optional 包装器对象，包装了类型 T  的对象，或者没包装任何，是更安全的方式，代替 T 的引用，
         * 注意只有在正确使用时才安全
         */
        /**
         *   使用 Optional 对象的值
         */
        Optional<String> first1 = Stream.of("abc", "bcd").filter(m -> m.length() > 2).findFirst();
        String value = first1.orElse("木有"); // 没有情况下默认值
        System.out.println(value);
        value = first1.orElseGet(() -> getDefault()); // 调用函数计算
        System.out.println(value);
        value = first1.orElseThrow(IllegalStateException::new); // 或者抛出异常
        System.out.println(value);
        // ifPresent(),如果存在，就消费这个值，比如增加到集合
        List<String> list1 = new ArrayList<>();
        first1.ifPresent(v -> list1.add(v)); // 等价的方式
        first1.ifPresent(list1::add);      // 等价的方式
        System.out.println(list1);
        // ifPresent()不会返回结果，如果想处理函数结果，应该使用map
        Optional<Boolean> flag = first1.map(list1::add);
        System.out.println(flag.orElse(false)); // 具有三种值，false true 和 first不存在的空值

        // get(),获得 Optional的值，不存在抛出NoSuchElementException异常
        // isPresent(); 如果，Optional不为空，返回true
        /**
         * 创建Optional的值
         */
        // Optional.of()
        Optional<String> create1 = Optional.of("zhouxudong");
        System.out.println(create1.orElse(""));
        // Optional.empty()
        create1 = Optional.empty(); // 创建空的
        //  System.out.println(create1.get());
        System.out.println(create1.orElse("创建一个空的"));
        // Optional.ofNullAble() 允许为空
        String string = null;
        create1 = Optional.ofNullable(string); // 第一种情况会抛出 NullPointerException异常
        System.out.println(create1.orElse("ofnullable"));
        // 用flatMap构建 Optional 的值

        /**
         * 收集结果
         */
        // forEach(),在流的每个结果调用函数，这是终结操作,按照的是任意顺序
        Stream<String> stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        stringStream.forEach(System.out::println);
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩").sorted();
        stringStream.forEach(m -> print(m));
        // forEachOrdered  按照流的顺序执行
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩").sorted();
        stringStream.forEachOrdered(n -> print(n));
        // 将结果集放到 array 数组,终结操作
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        String[] array1 = stringStream.toArray(String[]::new);
        System.out.println(Arrays.deepToString(array1));
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        Object[] array2 = stringStream.toArray(); // 得到泛型数组
        System.out.println(Arrays.deepToString(array2));
        // Collections
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        List<String> list2 = stringStream.collect(Collectors.toList());
        System.out.println("list: " + list2);
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        Set<String> set = stringStream.collect(Collectors.toSet());
        System.out.println("set:" + set);
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        TreeSet<String> treeSet = stringStream.collect(Collectors.toCollection(TreeSet::new));
        System.out.println("treeSet:" + treeSet);
        // 连接起来
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        String collectJoin = stringStream.collect(Collectors.joining());
        System.out.println("collectJoin:" + collectJoin);
        // 通过分隔符连
        stringStream = Stream.of("tom", "tom", "周旭东", "周旭东", "胡倩");
        collectJoin = stringStream.collect(Collectors.joining(","));
        System.out.println("collectJoin:" + collectJoin);
        // Colleations.summarizingInt(long/double等)，可以产生最大、最小、平均、求和等
        Stream<Integer> integerStream2 = Stream.of(12, 3, 1, 66, 23);
        IntSummaryStatistics summaryStatistics = integerStream2.collect(Collectors.summarizingInt(Integer::intValue));
        long sum = summaryStatistics.getSum();
        Integer max1 = summaryStatistics.getMax();
        System.out.println(sum);
        System.out.println(max1);
        /**
         * 收集结果到映射表中，放到map里边  collection.toMap(key,value),用函数可以得到
         */
        //   Map<Integer,String> 单一的属性映射 ID--》name
        Map<Integer, String> toMapName = people().collect(Collectors.toMap(Person::getId, Person::getName));
        System.out.println("toMapName:" + toMapName);
        //  Map<Integer,Person> 利用 Function.identity(),将对象作为值映射
        Map<Integer, Person> toMapPerson = people().collect(Collectors.toMap(Person::getId, Function.identity()));
        System.out.println("toMapPerson:" + toMapPerson);

    }

}

class Person {
    private int id;
    private String name;

    public Person() {
    }

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return getClass().getName() + "[id=" + id + ",name=" + name + "]";
    }
}