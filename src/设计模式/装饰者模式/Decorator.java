package 设计模式.装饰者模式;

public abstract class Decorator implements Human {
    private Human human;

    public Decorator(Human human) {
        this.human = human;
    }

    @Override
    public void byHouse() {
        human.byHouse();
    }

    @Override
    public void byCar() {
        human.byCar();
    }
}
