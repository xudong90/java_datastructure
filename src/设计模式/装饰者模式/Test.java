package 设计模式.装饰者模式;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Test {
    public static void main(String[] args) {
        Human human = new Person();
        Decorator decorator = new DecoratorOne(human);
        decorator = new DecoratorTwo(decorator);
        decorator = new DecoratorThree(decorator);
        decorator.byHouse();

    }
}
