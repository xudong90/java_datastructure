package 设计模式.装饰者模式;

public class DecoratorTwo extends Decorator {
    public DecoratorTwo(Human human) {
        super(human);
    }

    public void whereHouse() {
        System.out.println("去北京买房子");
    }

    public void whereCar() {
        System.out.println("去上海买车");
    }

    @Override
    public void byHouse() {
        super.byHouse();
        whereHouse();
    }

    @Override
    public void byCar() {
        super.byCar();
        whereCar();
    }
}
