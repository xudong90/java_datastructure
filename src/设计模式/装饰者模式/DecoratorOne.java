package 设计模式.装饰者模式;

public class DecoratorOne extends Decorator {

    public DecoratorOne(Human human) {
        super(human);
    }

    public void whereToByHouse() {
        System.out.println("要去哪里买房子");
    }

    public void whereToByCar() {
        System.out.println("要去哪里买车");
    }

    @Override
    public void byHouse() {
        super.byHouse();
        whereToByHouse();
    }

    @Override
    public void byCar() {
        super.byCar();
        whereToByCar();
    }
}
