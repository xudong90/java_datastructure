package 设计模式.装饰者模式;

public class DecoratorThree extends Decorator {
    public DecoratorThree(Human human) {
        super(human);
    }

    public void whereByHouse() {
        System.out.println("买下了故宫");
    }

    public void whereByCar() {
        System.out.println("买了漏油的奔驰");
    }

    @Override
    public void byHouse() {
        super.byHouse();
        whereByHouse();
    }

    @Override
    public void byCar() {
        super.byCar();
        whereByCar();
    }
}
