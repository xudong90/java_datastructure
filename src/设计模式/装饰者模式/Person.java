package 设计模式.装饰者模式;

public class Person implements Human {
    @Override
    public void byHouse() {
        System.out.println("我要买房子，给点建议");
    }

    @Override
    public void byCar() {
        System.out.println("我要买车，给点建议");
    }
}
