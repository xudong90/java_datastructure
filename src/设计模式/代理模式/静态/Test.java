package 设计模式.代理模式.静态;

public class Test {
    public static void main(String[] args) {
        PersonDao personProxy = new PersonProxy();
        personProxy.buyHouse();
        personProxy.buyCar();
    }
}
