package 设计模式.代理模式.静态;

public class PersonProxy implements PersonDao {

    private PersonDao personDao;

    public PersonProxy() {
        this.personDao = new PersonImpl();
    }

    @Override
    public void buyHouse() {
        System.out.println("代理对象执行开始 buyhouse");
        personDao.buyHouse();
        System.out.println("代理对象执行结束 buyhouse");

    }

    @Override
    public void buyCar() {
        System.out.println("代理对象执行开始 buyCar");
        personDao.buyCar();
        System.out.println("代理对象执行结束 buyCar");
    }
}
