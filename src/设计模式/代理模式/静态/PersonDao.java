package 设计模式.代理模式.静态;

public interface PersonDao {
    public void buyHouse();

    public void buyCar();
}
