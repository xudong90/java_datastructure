package 设计模式.代理模式.静态;

public class PersonImpl implements PersonDao {
    @Override
    public void buyHouse() {
        System.out.println("买房子");
    }

    @Override
    public void buyCar() {
        System.out.println("买车");
    }
}
