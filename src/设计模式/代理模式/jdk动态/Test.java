package 设计模式.代理模式.jdk动态;

import java.lang.reflect.Proxy;

public class Test {
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("张三");
        // 创建被代理的对象
        PersonImpl personImpl = new PersonImpl();
        // 创建动态代理调用方法的处理器
        PersonProxy personProxy = new PersonProxy(personImpl);
        // 创建动态代理对象
        PersonDao personDao1 = (PersonDao) Proxy.newProxyInstance(personProxy.getClass().getClassLoader(), personImpl.getClass().getInterfaces(), personProxy);
        personDao1.buyHouse(person);
        personDao1.buyCar(person);
    }
}
