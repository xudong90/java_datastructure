package 设计模式.代理模式.jdk动态;


public class PersonImpl implements PersonDao {


    @Override
    public void buyHouse(Person person) {
        System.out.println(person.getName() + "买房子");
    }

    @Override
    public void buyCar(Person person) {
        System.out.println(person.getName() + "买车");
    }
}
