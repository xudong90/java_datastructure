package 设计模式.代理模式.jdk动态;

public interface PersonDao {
    public void buyHouse(Person person);

    public void buyCar(Person person);
}
