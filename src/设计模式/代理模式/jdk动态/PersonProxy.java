package 设计模式.代理模式.jdk动态;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class PersonProxy implements InvocationHandler {
    private Object target;

    public PersonProxy(Object target) {
        this.target = target;
    }

    public Object getTarget() {
        return target;
    }

    public void setTarget(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName() + "   start");
        Object obj = method.invoke(this.target, args);
        System.out.println(method.getName() + "   end");
        System.out.println(((Person) args[0]).getName() + "   end");

        return obj;
    }
}
