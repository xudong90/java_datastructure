package 设计模式.观察者模式;

import java.util.ArrayList;
import java.util.List;

public class SubjectImpl implements Subject {
    private List<Observer> list = new ArrayList<>();
    private String message;

    @Override
    public void addObserver(Observer o) {
        list.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        list.remove(o);
    }

    @Override
    public void notifyObserver() {
        for (Observer o : list) {
            o.getMessage(message);
        }
    }

    public void setMessage(String s) {
        this.message = s;
        System.out.println("发送消息：" + s);
        notifyObserver();
    }
}
