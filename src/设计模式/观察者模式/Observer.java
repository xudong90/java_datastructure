package 设计模式.观察者模式;

public interface Observer {
    /**
     * 观察者收信息
     */
    void getMessage(String s);
}
