package 设计模式.观察者模式;

public interface Subject {
    /**
     * 添加观察者
     *
     * @param o
     */
    void addObserver(Observer o);

    /**
     * 移除观察者
     *
     * @param o
     */
    void removeObserver(Observer o);

    /**
     * 通知观察者
     */
    void notifyObserver();
}
