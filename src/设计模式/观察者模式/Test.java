package 设计模式.观察者模式;

public class Test {
    public static void main(String[] args) {
        SubjectImpl subject = new SubjectImpl();
        User observer1 = new User();
        observer1.setName("tony");
        User observer2 = new User();
        observer2.setName("stark");
        User observer3 = new User();
        observer3.setName("hoak");

        subject.addObserver(observer1);
        subject.addObserver(observer2);
        subject.addObserver(observer3);

        subject.setMessage("GO 语言以后会超越 Java");
        System.out.println("================");
        subject.removeObserver(observer1);
        subject.setMessage("放屁，Java无敌，你们都是渣渣");


    }
}
