package 设计模式.观察者模式;

public class User implements Observer {
    private String name;
    private String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void getMessage(String s) {
        this.message = s;
        read();
    }

    public void read() {
        System.out.println(name + "收到消息：" + message);
    }
}
