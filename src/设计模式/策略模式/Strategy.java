package 设计模式.策略模式;

/**
 * 定义抽象策略角色
 * 类似 Comparator接口
 */
public interface Strategy {
    /**
     * 计算 ，具体规则不明
     *
     * @param num1
     * @param num2
     * @return
     */
    public int caculate(int num1, int num2);
}
