package 设计模式.策略模式;

public class StrategyImple implements Strategy {
    private Strategy strategy;

    public StrategyImple(Strategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public int caculate(int num1, int num2) {
        return strategy.caculate(num1, num2);
    }
}
