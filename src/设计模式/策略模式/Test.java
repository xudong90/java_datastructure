package 设计模式.策略模式;

public class Test {
    public static void main(String[] args) {
        Strategy strategy = new AddStrategy();
        Strategy strategy1 = new ReduceStrategy();
        StrategyImple strategyImple = new StrategyImple(strategy);
        int result = strategyImple.caculate(4, 2);
        System.out.println(result);
        strategyImple = new StrategyImple(strategy1);
        result = strategyImple.caculate(4, 2);
        System.out.println(result);
    }
}
