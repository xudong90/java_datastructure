package 设计模式.策略模式;

public class ReduceStrategy implements Strategy {
    @Override
    public int caculate(int num1, int num2) {
        return num1 - num2;
    }
}
