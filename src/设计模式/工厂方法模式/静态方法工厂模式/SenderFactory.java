package 设计模式.工厂方法模式.静态方法工厂模式;


public class SenderFactory {
    public SenderFactory() {

    }

    public static Sender produceSenderMsg() {
        return new SendMsg();
    }

    public static Sender produceSenderEmail() {
        return new SendEmail();
    }
}
