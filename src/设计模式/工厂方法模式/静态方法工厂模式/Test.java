package 设计模式.工厂方法模式.静态方法工厂模式;

public class Test {
    public static void main(String[] args) {
        Sender sender = SenderFactory.produceSenderEmail();
        sender.send();
    }
}
