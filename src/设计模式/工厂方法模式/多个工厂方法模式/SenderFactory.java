package 设计模式.工厂方法模式.多个工厂方法模式;


public class SenderFactory {
    public SenderFactory() {

    }

    public Sender produceSenderMsg() {
        return new SendMsg();
    }

    public Sender produceSenderEmail() {
        return new SendEmail();
    }
}
