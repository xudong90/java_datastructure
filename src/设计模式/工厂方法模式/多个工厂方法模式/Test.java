package 设计模式.工厂方法模式.多个工厂方法模式;

public class Test {
    public static void main(String[] args) {
        SenderFactory senderFactory = new SenderFactory();
        Sender sender = senderFactory.produceSenderEmail();
        sender.send();
    }
}
