package 设计模式.工厂方法模式.普通工厂方法模式;

public class SenderFactory {

    public Sender produceSender(String str) {
        if ("msg".equals(str)) {
            return new SendMsg();
        } else if ("Email".equals(str)) {
            return new SendEmail();
        } else {
            System.out.println("类型不匹配");
            return null;
        }
    }
}
