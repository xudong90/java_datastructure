package 设计模式.工厂方法模式.普通工厂方法模式;

public class SendEmail implements Sender {
    @Override
    public void send() {
        System.out.println("发送电子邮件，请查收");
    }
}
