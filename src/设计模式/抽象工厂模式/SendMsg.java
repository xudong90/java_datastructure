package 设计模式.抽象工厂模式;


public class SendMsg implements Sender {
    @Override
    public void send() {
        System.out.println("发送一条短信，收到了吗？");
    }
}
