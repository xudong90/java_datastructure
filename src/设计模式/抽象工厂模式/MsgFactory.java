package 设计模式.抽象工厂模式;

public class MsgFactory implements Produce {
    @Override
    public Sender produce() {
        return new SendMsg();
    }
}
