package 设计模式.适配器模式.类适配器;

public class ChineseHuman extends Pigeon implements Human {
    @Override
    public void run() {
        System.out.println("跑起来了");
        fly();
    }
}
