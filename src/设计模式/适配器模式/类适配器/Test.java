package 设计模式.适配器模式.类适配器;

public class Test {
    public static void main(String[] args) {
        Human human = new ChineseHuman(); // run中没有我们的想要的方法，通过类适配器，访问run 的时候，访问了想要的方法
        human.run();
    }
}
