package 设计模式.适配器模式.对象适配器;

/**
 * 鸽子
 */
public class Pigeon implements Bird {
    @Override
    public void fly() {
        System.out.println("飞起来了");
    }
}
