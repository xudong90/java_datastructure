package 设计模式.适配器模式.对象适配器;

public class ChineseHuman implements Human {
    private Bird bird;

    public ChineseHuman(Bird bird) {
        this.bird = bird;
    }

    @Override
    public void run() {
        System.out.println("跑起来了");
        bird.fly();
    }
}
