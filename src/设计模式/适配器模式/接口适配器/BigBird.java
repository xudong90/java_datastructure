package 设计模式.适配器模式.接口适配器;

public abstract class BigBird implements Bird {
    @Override
    public void fly() {
    }

    @Override
    public void eatWorm() {
    }

    @Override
    public void createEgg() {
    }
}
