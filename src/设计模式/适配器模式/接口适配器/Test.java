package 设计模式.适配器模式.接口适配器;

public class Test {
    public static void main(String[] args) {
        Bird bird = new Pigeon();
        bird.fly();
        bird.createEgg();
    }
}
