package 设计模式.适配器模式.接口适配器;

public class Pigeon extends BigBird {
    @Override
    public void fly() {
        super.fly();
        System.out.println("飞起来了");
    }
}
