package 设计模式.适配器模式.接口适配器;

public interface Bird {
    /**
     * 飞
     */
    public void fly();

    /**
     * 吃虫子
     */
    public void eatWorm();

    /**
     * 下蛋
     */
    public void createEgg();
}
