package 排序算法.插入排序;

import java.util.Arrays;

/**
 * 缩减增量排序，不过是 1 ，不用缩减，特殊
 */
public class Demo {
    public static void main(String[] args) {
        Integer[] array = {14, 2, 33, 4, 1, 56, 147, 1, 8};
        sortAsc(array);
        System.out.println(Arrays.deepToString(array));
        sortSesc(array);
        System.out.println(Arrays.deepToString(array));

        for(int p=1;p<array.length;p++){
            int temp = array[p];
            int j =p;
            while(j>0 && array[j]<array[j-1]){
                array[j] = array[j-1];
                j--;
            }
            array[j] = temp;
        }
    }

    /**
     * 插入排序
     *
     * @param array
     */
    public static void sortAsc(Integer[] array) {
        int j; // 预先定义插入的位置
        for (int i = 1; i < array.length; i++) { // 从第二个位置开始
            Integer temp = array[i];

            for (j = i; j > 0 && temp.compareTo(array[j - 1]) < 0; j--) {
                array[j] = array[j - 1];
            }
            array[j] = temp;
        }
    }

    public static void sortSesc(Integer[] array) {
        int j;
        for (int i = 0; i < array.length; i++) {
            j = i;
            Integer temp = array[i];
            while (j > 0 && temp.compareTo(array[j - 1]) > 0) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = temp;
        }
    }
}
