package 树.AVL平衡二叉查找树;

public class AVLTree<T extends Comparable<? super T>> {
    public static class AVLNode<T> {
        T element;
        AVLNode left;
        AVLNode right;
        int height;

        public AVLNode(T element) {
            this.element = element;
            this.left = null;
            this.right = null;
            this.height = 0;
        }

        public AVLNode(T element, AVLNode<T> left, AVLNode<T> right) {
            this.element = element;
            this.left = left;
            this.right = right;
            this.height = 0;
        }

    }

    public int height(AVLNode<T> t) {
        return t == null ? -1 : t.height;
    }

    // LL 单旋转，顺时针的，根节点的左子树向上，根节点向下，1. 左子树的右子树，变为根节点左子树，2. 同时，根节点变为该左子树的右子树
    private AVLNode<T> LL(AVLNode<T> k2) {
        AVLNode<T> k1 = k2.left;  // k2作为根节点，k1时左子树
        k2.left = k1.right; // 1. 左子树的右子树，变为根节点左子树
        k1.right = k2;// 2. 根节点变为该左子树的右子树
        k2.height = Math.max(height(k2.left), height(k2.right)) + 1;
        k1.height = Math.max(height(k1.left), k2.height) + 1;
        return k1;
    }

    // RR 单旋转，逆时针的，根节点的右子树向上，跟节点向下，右子树的左子树，变为根节点右子树，同时，根节点变为该右子树的左子树
    private AVLNode<T> RR(AVLNode<T> k2) {
        AVLNode<T> k1 = k2.right;  // k2作为根节点，为右子树
        k2.right = k1.left; // 1. 左子树的右子树，变为根节点左子树
        k1.left = k2;// 2. 根节点变为该右子树的左子树
        k2.height = Math.max(height(k2.left), height(k2.right)) + 1;
        k1.height = Math.max(height(k1.left), height(k1.right)) + 1;
        return k1;
    }

    // LR 双旋转，左子树先RR,根节点再LL
    public AVLNode<T> LR(AVLNode<T> k2) {
        k2.left = RR(k2.left);
        return LL(k2);
    }

    // Rl 双旋转，右子树先LL ,根节点再RR
    public AVLNode<T> RL(AVLNode<T> k2) {
        k2.right = LL(k2.right);
        return RR(k2);
    }

    public AVLNode<T> insert(T x, AVLNode<T> t) {
        if (t == null)
            return new AVLNode<>(x, null, null);
        int compareResult = x.compareTo(t.element);
        if (compareResult < 0)
            t.left = insert(x, t.left);
        else if (compareResult > 0)
            t.right = insert(x, t.right);
        else
            ; // 如果相同，说明插入的值已经存在，可以什么都不做，或者做一些其他操作
        t = balance(t); // 就比一般的都一个平衡方法
        return t;
    }

    public AVLNode<T> balance(AVLNode<T> t) {
        if (t == null)
            return t;
        if (height(t.left) - height(t.right) > 1) { // L 左子树深度比右子树多，不平衡
            if (height(t.left.left) >= height(t.left.right)) { // L 左子树的左子树深度>=左子树的右子树深度
                t = LL(t);  // 满足LL单旋转
            } else { // R 左子树的左子树深度  <  左子树的右子树深度
                t = LR(t); // 满足 LR 双旋转
            }
        }
        if (height(t.right) - height(t.left) > 1) { // R 右子树深度比左子树多，不平衡
            if (height(t.right.right) >= height(t.right.left)) { // R 右子树的右子树深度>=右子树的左子树深度
                t = RR(t);  // 满足RR单旋转
            } else { // L 右子树的右子树深度  <  右子树的左子树深度
                t = RL(t); // 满足 RL 双旋转
            }
        }
        t.height = Math.max(height(t.left), height(t.right)) + 1;
        return t;
    }

    // 用中序打印的
    public void printTree(AVLNode<T> t) {
        if (t != null) {
            printTree(t.left);
            System.out.println(t.element);
            printTree(t.right);
        }
    }
}
