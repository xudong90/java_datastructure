package 树.二叉树;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree("A");
        BinaryNode node11 = binaryTree.add(binaryTree.root, "B", true);
        BinaryNode node12 = binaryTree.add(binaryTree.root, "C", false);
        BinaryNode node21 = binaryTree.add(node11, "D", true);
        BinaryNode node22 = binaryTree.add(node11, "E", false);
        BinaryNode node23 = binaryTree.add(node12, "F", true);
        BinaryNode node24 = binaryTree.add(node12, "G", false);
        BinaryNode node31 = binaryTree.add(node21, "H", true);
        BinaryNode node32 = binaryTree.add(node21, "I", false);
        // 前序遍历
        System.out.println("前序遍历结构为：");
        List<BinaryNode> list1 = binaryTree.beforeAll();
        for (BinaryNode binaryNode : list1) {
            System.out.print(binaryNode.binaryNode);
        }
        // 中序遍历
        System.out.println("\n中序遍历结构为：");
        List<BinaryNode> list2 = binaryTree.middleAll();
        for (BinaryNode binaryNode : list2) {
            System.out.print(binaryNode.binaryNode);
        }
        // 后序遍历
        System.out.println("\n后序遍历结构为：");
        List<BinaryNode> list3 = binaryTree.afterAll();
        for (BinaryNode binaryNode : list3) {
            System.out.print(binaryNode.binaryNode);
        }
    }
}
