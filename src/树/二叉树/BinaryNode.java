package 树.二叉树;

public class BinaryNode {
    Object binaryNode;
    BinaryNode left;
    BinaryNode right;

    public BinaryNode(Object binaryNode) {
        this.binaryNode = binaryNode;
    }

    /**
     * 构造一个节点
     *
     * @param binaryNode 节点数据
     * @param left       左子节点
     * @param right      右子节点
     */
    public BinaryNode(Object binaryNode, BinaryNode left, BinaryNode right) {
        this.binaryNode = binaryNode;
        this.left = left;
        this.right = right;
    }
}
