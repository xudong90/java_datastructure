package 树.二叉树;

import java.util.ArrayList;
import java.util.List;

public class BinaryTree<T> {
    BinaryNode root; //表示根节点

    public BinaryTree(Object root) {
        this.root = new BinaryNode(root);
    }

    /**
     * 添加节点,先确定插入的位置，再判断是能插入
     *
     * @return
     */
    public BinaryNode add(BinaryNode parentNode, Object data, boolean isLeft) {
        if (parentNode == null)
            throw new RuntimeException("父节点为空，无法添加子节点");
        if (isLeft && parentNode.left != null)
            throw new RuntimeException("左子节点已经存在，添加失败");
        if (!isLeft && parentNode.right != null)
            throw new RuntimeException("右子节点已经存在，添加失败");
        BinaryNode binaryNode = new BinaryNode(data); // 定义一个新节点
        if (isLeft) {
            parentNode.left = binaryNode;
        } else {
            parentNode.right = binaryNode;
        }
        return binaryNode;
    }

    /**
     * 前序遍历 ： 父节点-->左子节点-->右子节点
     *
     * @return
     */
    public List<BinaryNode> beforeAll() {
        return beforeQuery(root);
    }

    public List<BinaryNode> beforeQuery(BinaryNode binaryNode) {
        List<BinaryNode> binaryNodeList = new ArrayList<>();
        binaryNodeList.add(binaryNode);
        if (binaryNode.left != null)
            binaryNodeList.addAll(beforeQuery(binaryNode.left)); // 递归调用
        if (binaryNode.right != null)
            binaryNodeList.addAll(beforeQuery(binaryNode.right)); // 递归调用
        return binaryNodeList;
    }

    /**
     * 中序遍历 ： 左子节点-->父节点-->右子节点
     *
     * @return
     */
    public List<BinaryNode> middleAll() {
        return middleQuery(root);
    }

    public List<BinaryNode> middleQuery(BinaryNode binaryNode) {
        List<BinaryNode> binaryNodeList = new ArrayList<>();
        if (binaryNode.left != null)
            binaryNodeList.addAll(middleQuery(binaryNode.left)); // 递归调用
        binaryNodeList.add(binaryNode);
        if (binaryNode.right != null)
            binaryNodeList.addAll(middleQuery(binaryNode.right)); // 递归调用
        return binaryNodeList;
    }

    /**
     * 后序遍历 ： 左子节点-->右子节点-->父节点
     *
     * @return
     */
    public List<BinaryNode> afterAll() {
        return afterQuery(root);
    }

    public List<BinaryNode> afterQuery(BinaryNode binaryNode) {
        List<BinaryNode> binaryNodeList = new ArrayList<>();
        if (binaryNode.left != null)
            binaryNodeList.addAll(afterQuery(binaryNode.left)); // 递归调用
        if (binaryNode.right != null)
            binaryNodeList.addAll(afterQuery(binaryNode.right)); // 递归调用
        binaryNodeList.add(binaryNode);
        return binaryNodeList;
    }

}
