package 树.二叉查找树;

import java.nio.BufferUnderflowException;

public class BinarySearchTree<T extends Comparable<? super T>> {
    // 1. 静态内部类定义 Node 节点
    private static class BinaryNode<T> {
        T element;
        BinaryNode left;
        BinaryNode right;

        public BinaryNode(T element) {
            this(element, null, null);
        }

        public BinaryNode(T element, BinaryNode left, BinaryNode right) {
            this.element = element;
            this.left = left;
            this.right = right;
        }
    }

    private BinaryNode<T> root;

    // 构造的是一个空树
    public BinarySearchTree() {
        root = null;
    }

    // 将 根节点设置为空，就清空了整个树
    public void makeEmpty() {
        root = null;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public boolean contains(T x) {
        return contains(x, root);
    }

    public T findMax() {
        if (isEmpty())
            throw new RuntimeException("树为空，不存在");
        return findMax(root).element;
    }

    public T findMin() {
        if (isEmpty())
            throw new RuntimeException("树为空，不存在");
        return findMin(root).element;
    }

    public void insert(T x) {
        root = insert(x, root);
    }

    public void remove(T x) {
        if (isEmpty())
            System.out.println("空树，无法删除");
        root = remove(x, root);

    }

    public void printTree() {
        if (isEmpty())
            System.out.println("空树，无法打印");
        else
            printTree(root);
    }

    public boolean contains(T x, BinaryNode<T> t) {
        if (t == null) // 节点不存在，那肯定是false
            return false;
        int compareResult = x.compareTo(t.element); //比较结果
        if (compareResult > 0) // 如果较大，就继续和该节点的右子节点比较
            return contains(x, t.right);
        else if (compareResult < 0) // 如果较小，就继续和该节点的左子节点比较
            return contains(x, t.left);
        else  // 等于0,说明一样大，存在，返回true
            return true;
    }

    //查找最大和最小的很简单，只需要找到最左的和最右的节点
    // 最小的用递归实现
    public BinaryNode<T> findMin(BinaryNode<T> t) {
        if (t == null)
            return null;
        else if (t.left == null)
            return t;
        else
            return findMin(t.left);
    }

    // 最大的用非递归
    public BinaryNode<T> findMax(BinaryNode<T> t) {
        if (t != null) {
            while (t.right != null) {
                t = t.right;
            }
        }
        return t;
    }

    // 插入返回根节点就行，就是和左右进行比较，一直到比较的节点没有了左子节点或右子节点，插入到最小的额位置
    public BinaryNode<T> insert(T x, BinaryNode<T> t) {
        if (t == null)
            return new BinaryNode(x, null, null);
        int compareResult = x.compareTo(t.element);
        if (compareResult < 0)
            t.left = insert(x, t.left);
        else if (compareResult > 0)
            t.right = insert(x, t.right);
        else
            ; // 如果相同，说明插入的值已经存在，可以什么都不做，或者做一些其他操作
        return t;
    }

    // 用中序打印的
    public void printTree(BinaryNode<T> t) {
        if (t != null) {
            printTree(t.left);
            System.out.println(t.element);
            printTree(t.right);
        }
    }

    public BinaryNode<T> remove(T x, BinaryNode<T> t) {
        if (t == null)
            return t;
        int compareResult = x.compareTo(t.element);
        if (compareResult < 0)
            t.left = remove(x, t.left);
        else if (compareResult > 0)
            t.right = remove(x, t.right);
        else if (t.left != null && t.right != null) // 两个子树
        {
            t.element = (T) findMin(t.right).element;
            t.right = remove(t.element, t.right);
        } else
            t = (t.left != null) ? t.left : t.right;
        return t;
    }
}
