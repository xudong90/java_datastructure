package 树.二叉查找树;

public class Test {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.insert(6);
        binarySearchTree.insert(2);
        binarySearchTree.insert(5);
        binarySearchTree.insert(1);
        binarySearchTree.insert(4);
        binarySearchTree.insert(3);
        binarySearchTree.insert(8);
        binarySearchTree.printTree();
        boolean c6 = binarySearchTree.contains(6);
        boolean c7 = binarySearchTree.contains(7);
        System.out.println("存在6？" + c6);
        System.out.println("存在7？" + c7);
        int min = (int) binarySearchTree.findMin();
        int max = (int) binarySearchTree.findMax();
        System.out.println("最小是：" + min);
        System.out.println("最大是：" + max);
        binarySearchTree.remove(2);
        binarySearchTree.printTree();


    }
}
