package 树;

import sun.management.FileSystem;

import javax.swing.tree.TreeNode;


/**
 * 1 定义节点类
 */
public class Node {
    TreeNode left;
    TreeNode right;
    String data;

    public Node() {
    }

    public Node(String data) {
        this.data = data;
    }

    public String getdata() {
        return data;
    }
}
