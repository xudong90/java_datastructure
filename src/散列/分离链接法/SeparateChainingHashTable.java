package 散列.分离链接法;

import java.util.LinkedList;
import java.util.List;

public class SeparateChainingHashTable<T> {
    private static final int DEFAULT_TABLE_SIZE = 101; // 默认表长度
    private List<T>[] theLists; // theLists 是数组，里边的元素是双向链表
    private int currentSize; // 已经散列的元素个数

    public SeparateChainingHashTable() {
        this(DEFAULT_TABLE_SIZE);
    }

    public SeparateChainingHashTable(int size) {
        theLists = new LinkedList[nextPrime(size)];
        for (int i = 0; i < theLists.length; i++) {
            theLists[i] = new LinkedList<>();
        }
    }

    // 此处 另加载因子等于 1
    public void insert(T x) {
        List<T> whichList = theLists[myHash(x)]; // myhash 函数的得出 插入元素 x 在 数组存放分下表，返回结果说就是 要插入的 链表
        if (!whichList.contains(x)) { // 先检查 是是否存在，然后 添加
            whichList.add(x);
            if (++currentSize > theLists.length) {
                reHash();
            }
        }
    }

    public void remove(T x) {
        List<T> whichList = theLists[myHash(x)];
        if (!whichList.contains(x)) {
            whichList.remove(x);
            currentSize--;
        }
    }

    public boolean contains(T x) {
        List<T> whichList = theLists[myHash(x)];
        return whichList.contains(x);
    }

    public void makeEmpty(T x) {
        for (int i = 0; i < theLists.length; i++) {
            theLists[i].clear();
            currentSize = 0;
        }
    }


    private void reHash() {
        List<T>[] oldLists = theLists;//复制一下一会要用    theLists在又一次new一个
        //对在散列的表同样进行素数修正
        theLists = new LinkedList[nextPrime(2 * theLists.length)];
        for (int i = 0; i < theLists.length; i++) {
            theLists[i] = new LinkedList<T>();
        }
        //把原来的元素拷贝到新的数组中  注意是把集合中的元素复制进去
        for (int i = 0; i < oldLists.length; i++) {
            for (T t : oldLists[i]) {
                insert(t);
            }
        }
    }

    // 将 hashCode 返回的int，转成适当的数组下标
    private int myHash(T x) {
        int hashVal = x.hashCode();
        hashVal %= theLists.length;
        if (hashVal < 0) {
            hashVal += theLists.length;
        }
        return hashVal;
    }

    // 求 下一个素数
    private static int nextPrime(int n) {
        while (!isPrime(n)) {
            n++;
        }
        return n;

    }

    // 判断是否是 素数
    private static boolean isPrime(int n) {
        int i = 1;
        while ((n % (i + 1)) != 0) {
            i++;
        }
        if (i == n - 1)
            return true;
        else
            return false;
    }
}
