package 散列.分离链接法;

public class Employee {
    private String name;
    private double salary;
    private int sex;


    public Employee() {
    }

    public Employee(String name, double salary, int sex) {
        this.name = name;
        this.salary = salary;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Employee && name.equals(((Employee) obj).name);
    }
}
