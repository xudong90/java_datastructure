package 散列;

import java.util.HashMap;

public class Demo {
    public static void main(String[] args) {
        System.out.println(hash("abc", 10007));
    }

    // 使用 字符串的ASCII 或 Unicode 编码，如果碰到 大的表，不能保证 关键字均匀分配
    public static int hash(String key, int tableSize) {
        int hashVal = 0;
        for (int i = 0; i < key.length(); i++) {
            hashVal += key.charAt(i);
        }
        System.out.println("hashVal is " + hashVal);
        return hashVal % tableSize;
    }

    // hk = ((k2)*37+k1)*37+k0
    public static int goodHash(String key, int tableSize) {
        int hashVal = 0;
        for (int i = 0; i < key.length(); i++) {
            hashVal = 37 * hashVal + key.charAt(i);
        }
        hashVal %= tableSize;
        if (hashVal < 0) {
            hashVal += tableSize;
        }
        return hashVal;

    }
}
