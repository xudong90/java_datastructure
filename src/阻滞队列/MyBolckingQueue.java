package 阻滞队列;

import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2019/12/18.
 */
public interface MyBolckingQueue<E> extends Queue<E> {

   // boolean add(E e);


  //  boolean offer(E e);
    // 定义阻滞方法，
    void put(E e) throws InterruptedException;

    //
    boolean offer(E e, long timeout, TimeUnit unit)
            throws InterruptedException;

    E take() throws InterruptedException;


    E poll(long timeout, TimeUnit unit)
            throws InterruptedException;


    int remainingCapacity();

    boolean remove(Object o);


    public boolean contains(Object o);

    int drainTo(Collection<? super E> c);


    int drainTo(Collection<? super E> c, int maxElements);
}
