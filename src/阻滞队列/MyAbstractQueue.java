package 阻滞队列;

import java.util.*;

/**
 * 抽象队列：
 * Created by Administrator on 2019/12/18.
 * 模板方法模式的应用，抽象类实现 add,remove,element 这些方法会报异常，同时增加 clear 和 addAll()
 * Queue 是 继承了 Collection 接口，同时定义了 六个方法
 * 添加元素：boolean add() 和 boolean offer(): 不同的是 当 队列满了 ，add 抛出异常，offer 返回 false
 * 删除元素： E remove() 和 E poll() : 不为空,返回被删除的元素，不同的是，当队列为空时，remove 抛出异常，poll 返回null
 * 获取头部元素：E element() 和 E peek();都返回头部的元素，但是不删除，如果队列为空，element 抛出异常，peek 返回null
 *
 * 本抽象模板类 实现了 add remove element ,等让他的实现类实现具体的 offer poll peek
 *
 */
public abstract class MyAbstractQueue<E> extends AbstractCollection<E> implements Queue<E>{

    public MyAbstractQueue() {
    }
    // 模板 方法
    public boolean add(E e) {
        if (offer(e))
            return true;
        else
        throw  new IllegalStateException("Queue full");
    }
    // 模板 方法
    public E remove() {
        E x = poll();
        if(x!= null)
            return x;
        else
            throw  new NoSuchElementException();
    }

    // 模板 方法
    public E element() {
        E x = peek();
        if (x != null)
            return x;
         else
             throw new NoSuchElementException();
    }
    // 将集合 c 添加到队列，如果为空，返回 空指针异常，如果 c 就是当前队列，即自己添加自己，返回非法异常，
    public boolean addAll(Collection<? extends E> c) {
        if(c == null)
            throw  new NullPointerException();
        if(c == this)
            throw new IllegalStateException();
        boolean modified = false;
        for (E e: c) {
            if(add(e))
                modified = true;
        }
        return modified;
    }


    public void clear() {
        while (poll()!=null); // 好巧妙的方法
    }



}
