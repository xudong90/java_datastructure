package 优先队列堆.二叉堆;

public class BinaryHeap<T extends Comparable<? super T>> {
    private static final int Default_CAPACITY = 10;
    private int currentSize; // 当前二叉堆中的元素个数，不是数组的大小
    private T[] array; // 二叉堆所在数组

    public BinaryHeap() {
        this(Default_CAPACITY);
    }

    public BinaryHeap(int capacity) {
        makeEmpty();
        enlargeArray(capacity);
    }

    /**
     * 二叉堆的构造方法
     *
     * @param items
     */
    public BinaryHeap(T[] items) {
        currentSize = items.length;
        array = (T[]) new Comparable[(currentSize + 2) * 11 / 10];
        int i = 1;
        for (T item : items)
            array[i++] = item;
        buildHeap();
    }

    private void buildHeap() {
        for (int i = currentSize / 2; i > 0; i--) {
            percolateDowm(i);
        }
    }

    private void enlargeArray(int newSize) {
        T[] oldArray = array;
        if (newSize < currentSize) {
            return;
        }
        array = (T[]) new Comparable[newSize];
        for (int i = 1; i <= currentSize; i++) {
            array[i] = oldArray[i];
        }
    }

    public void insert(T x) {
        if (currentSize == array.length - 1) {
            enlargeArray(2 * array.length - 1);
        }
        int hole = ++currentSize;
        for (array[0] = x; x.compareTo(array[hole / 2]) < 0; hole /= 2) {
            array[hole] = array[hole / 2];
        }
        array[hole] = x;
    }

    public T findMin() {
        return array[1];
    }

    public T deleteMin() {
        if (isEmpty()) {
            throw new RuntimeException("堆为空");
        }
        T minnum = findMin();
        array[1] = array[currentSize--];
        percolateDowm(1);
        return minnum;
    }

    public boolean isEmpty() {
        return currentSize == 0;

    }

    public void makeEmpty() {
        for (int i = 0; i < currentSize; i++) {
            array[currentSize] = null;
        }
        currentSize = 0;
    }

    public void percolateDowm(int hole) {
        int child;
        T tmp = array[hole];  //暂存空洞节点的值

        for (; hole * 2 <= currentSize; hole = child) { //对子树遍历进行
            child = hole * 2;
            if (child != currentSize && array[child + 1].compareTo(array[child]) < 0) //找出两个子节点中的较小者
                child++;
            if (array[child].compareTo(tmp) < 0)  //子节点较小者与空洞值相比
                array[hole] = array[child];
            else
                break;
        }
        array[hole] = tmp;
    }

    public T[] getArray() {
        return array;
    }
}
