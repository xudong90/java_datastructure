package 优先队列堆.二叉堆;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {

        BinaryHeap<Integer> binaryHeap = new BinaryHeap<>();
        binaryHeap.insert(13);
        binaryHeap.insert(21);
        binaryHeap.insert(16);
        binaryHeap.insert(24);
        binaryHeap.insert(31);
        binaryHeap.insert(19);
        binaryHeap.insert(68);
        binaryHeap.insert(65);
        binaryHeap.insert(26);
        binaryHeap.insert(32);
        Comparable[] array = binaryHeap.getArray();
        System.out.println("单个插入的：" + Arrays.deepToString(array));
        binaryHeap.deleteMin();
        System.out.println("删除最小后：" + Arrays.deepToString(array));
        Integer[] items = {150, 80, 40, 30, 10, 70, 110, 100, 20, 90, 60, 50, 120, 140, 130};
        BinaryHeap<Integer> binaryHeap2 = new BinaryHeap<>(items);
        Comparable[] array2 = binaryHeap2.getArray();
        System.out.println("用数组构造的：" + Arrays.deepToString(array2));


    }
}
